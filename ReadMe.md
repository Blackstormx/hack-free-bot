# Hack-Free Bot - Le bot officiel d'Hack-Free sur Tox !  

## Compiler, nettoyer et commit :

Pour **compiler** il suffit d'utiliser le script **run.sh**
```
#!bash
./run.sh
```

Pour **nettoyer** il suffit d'utiliser le script **clean.sh**
```
#!bash
./clean.sh
```

Pour **commit** il suffit d'utiliser le script **commit.sh**
```
#!bash
./commit.sh       # Commit sans envoyer sur BitBucket.
./commit.sh -push # Commit + envoie sur BitBucket.
./commit.sh -pull # Récupérer les modifications via BitBucket sans commit.
```

## En cours de développement :
[**x**] Notification sur le groupe lors d'un nouveau message sur le forum.

## Idées :
_N'hésitez pas à proposer vos idées_

## Correction de bugs :
_Pas encore de bugs à fixer_