#!/bin/sh
./clean.sh

if [ "$1" = "-pull" ]
then
    git pull -f origin master
else
    git add . -A

    echo -n "Enter the commit message and press [ENTER]: "
    read msg
    git commit -m "$msg"
fi

if [ "$1" = "-push" ]
then
    git push -f origin master
fi
