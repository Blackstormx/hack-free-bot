#![allow(unused_variables)]
extern crate rstox;
extern crate time;

use rstox::core::*;
use time::*;
use std::ops::{Add};

use std::path::Path;
use std::fs::File;
use std::io::prelude::*;
use std::thread::*;

// Static options.
static BOOTSTRAP_IP: &'static str = "195.154.119.113";
static BOOTSTRAP_PORT: u16 = 33445;
static BOOTSTRAP_KEY: &'static str = "E398A69646B8CEACA9F0B84F553726C1C49270558C57DF5F3C368F05A7D71354";
static HFB_PROFILE: &'static str = "./profiles/hfb.tox";
static BOT_NAME: &'static str = "Hack-Free Bot";

// Static array.
static SUPER_USERS: &'static [&'static str] = &[
    "F7A425D7064A8DC1353E5C07E908C0CCF83947916AE519080D8F9DF8B1DCD243",
    "10EC72CA34FB138A6246B48DA8A616D008400F95D61201EB3DD70502601C6820"
];

#[derive(Clone, PartialEq, Debug)]
struct Peer
{
    last_name: String,
    name: String,
    public_key: PublicKey
}

#[derive(Clone, PartialEq, Debug)]
struct BotOptions
{
    greetings: bool,
    name_changes: bool,
    topic_lock: bool,
    topic_to_lock: String,
    commands: bool
}

fn main() {
    let mut m: Tox = init_tox();
    let mut timestamp: time::Timespec = time::get_time();
    let mut group_id = make_group_tox(&mut m); // 0
    let mut peers: Vec<Peer> = Vec::new(); // peers is a Vector of Peer's.
    let mut opts = BotOptions {
        greetings: true,
        name_changes: true,
        topic_lock: false,
        topic_to_lock: m.group_get_title(group_id).unwrap(),
        commands: true
    };

    println!("ID: {}", m.get_address());
    println!("[TIME] {:?}", timestamp);

    save_point_tox(&m);

    loop {
        for ev in m.iter() {
            println!("[DEBUG] {:?}", ev);
            //

            match ev {

                FriendRequest(cid, _) => {
                    drop(m.add_friend_norequest(&cid));
                    save_point_tox(&m);
                },

                FriendMessage(cid, kind, data) => {
                    let fnum: i32 = cid as i32;

                    match &*data
                    {
                        "hf4ever" =>
                        {
                            drop(m.invite_friend(fnum, group_id));
                        },
                        _ => { /* IGNORE ALL */ }
                    }
                },

                GroupInvite(fid, kind, data) =>
                {
                    let fnum: u32 = fid as u32;
                    let pkey = format!("{}", match m.get_friend_public_key(fnum) {
                        Some(k) => k,
                        None => return,
                    });

                    if is_su(pkey)
                    {
                        timestamp = time::get_time();
                        println!("[TIME] {:?}", timestamp);
                        group_id = m.join_groupchat(fid, &data).unwrap();
                    }

                    println!("Je crois que {} m'a pris pour une pute...", fid);
                },

                GroupMessage(gid, pid, message) =>
                {


                    let cmd = message.to_string();
                    let name = match m.group_peername(gid, pid) {
                        Some(n) => n.to_string(),
                        None => pid.to_string()
                    };
                    let pkey_o: PublicKey = match m.group_peer_pubkey(gid, pid) {
                        Some(pk) => pk,
                        None => {
                            PublicKey { raw: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] }
                        }
                    };
                    let pkey = format!("{}", match m.group_peer_pubkey(gid, pid) {
                        Some(k) => k,
                        None => {
                            PublicKey { raw: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] }
                        }
                    });

                    let peer = match peers.iter_mut().find(|x| x.public_key == pkey_o)
                    {
                        Some(p) => p,
                        _ => continue
                    };

                    /*if peer.public_key != m.get_public_key()
                    {
                        m.set_name(BOT_NAME).unwrap();
                    }*/

                    match &*cmd
                    {
                        "aide" =>
                        {
                            if opts.commands {
                                let message = "est développé par SkyzohKey et Ogromny avec le langage Rust";
                                drop(m.group_action_send(gid, &message));
                            }
                        },
                        "id" =>
                        {
                            if is_su(pkey)
                            {
                                let pub_key = format!("{}", m.get_address());
                                drop(m.group_message_send(gid, &pub_key));
                            }
                        },
                        "pubkey" =>
                        {
                            if opts.commands {
                                drop(m.group_message_send(gid, &pkey));
                            }
                        },
                        "lastname" =>
                        {
                            if opts.commands {
                                if peer.last_name != "-1"
                                {
                                    drop(m.group_message_send(gid, &peer.last_name));
                                }
                                else
                                {
                                    let msg = "Tu n'as pas encore changé de pseudo. '-'";
                                    drop(m.group_message_send(gid, &msg));
                                }
                            }
                        },

                        // Options
                        opt @ "opt:namechanges 0" | opt @ "opt:namechanges 1" =>
                        {
                            if is_su(pkey) {
                                let val: bool = opt.contains("1");
                                opts.name_changes = val;
                            }
                        },
                        opt @ "opt:greetings 0" | opt @ "opt:greetings 1" =>
                        {
                            if is_su(pkey) {
                                let val: bool = opt.contains("1");
                                opts.greetings = val;
                            }
                        },
                        opt @ "opt:commands 0" | opt @ "opt:commands 1" =>
                        {
                            if is_su(pkey) {
                                let val: bool = opt.contains("1");
                                opts.commands = val;
                            }
                        },
                        opt @ "opt:topiclock 0" | opt @ "opt:topiclock 1" =>
                        {
                            if is_su(pkey) {
                                let val: bool = !!opt.contains("1");
                                opts.topic_lock = val;
                                opts.topic_to_lock = m.group_get_title(gid).unwrap();
                            }
                        },

                        _ => { /* Ignore. */ }
                    };
                },

                GroupNamelistChange(gid, pid, kind) =>
                {
                    let name = match m.group_peername(gid, pid) {
                        Some(n) => n,
                        None => pid.to_string()
                    };
                    let pkey_o: PublicKey = match m.group_peer_pubkey(gid, pid) {
                        Some(pk) => pk,
                        None => {
                            PublicKey { raw: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] }
                        }
                    };
                    let pkey = format!("{}", match m.group_peer_pubkey(gid, pid) {
                        Some(k) => k,
                        None => {
                            PublicKey { raw: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] }
                        }
                    });

                    match kind
                    {
                        ChatChange::PeerAdd =>
                        {
                            if peers.iter().any(|x| x.public_key == pkey_o)
                            {
                                peers.retain(|x| x.public_key != pkey_o); // Retains peer that doesn't match the public key.
                            }

                            peers.push(Peer { // Add a new peer.
                                name: "-1".to_string(),
                                last_name: "-1".to_string(),
                                public_key: pkey_o
                            });
                        },
                        ChatChange::PeerDel =>
                        {
                            peers.retain(|x| x.public_key != pkey_o); // Retains peer that doesn't match the public key.
                            //println!("[PEERS-DEL] {:?}", &peers);
                        },
                        ChatChange::PeerName =>
                        {
                            match m.group_peername(gid, pid)
                            {
                                Some(pname) =>
                                {
                                    if peers.iter().any(|x| x.public_key == pkey_o) // If a peer with this public key is in the Vec<Peer> :
                                    {
                                        let peer = peers.iter_mut().find(|x| x.public_key == pkey_o).unwrap();

                                        *peer = Peer {
                                            last_name: peer.name.to_string(),
                                            name: pname.to_string(),
                                            public_key: pkey_o
                                        };

                                        // Ugly way to welcome only people arrived after the bot,
                                        // Needed to fight against spam on joining the group. :)
                                        if timestamp.add(Duration::seconds(3)) < time::get_time()
                                        {
                                            if peer.last_name == "-1"
                                            {
                                                if opts.greetings {
                                                    let msg = format!("Bienvenue {}, si tu as besoin d'aide il te suffit d'écrire \"aide\" :)", pname);
                                                    drop(m.group_message_send(gid, &msg));
                                                }
                                            }
                                            else
                                            {
                                                if opts.name_changes {
                                                    let msg = format!("> {} s'appel maintenant {}.", peer.last_name, pname);
                                                    drop(m.group_message_send(gid, &msg));
                                                }
                                            }
                                        }
                                    }

                                    //println!("[PEERS-NAME] {:?}", &peers);
                                },
                                None => { /* */ },
                            };
                        }
                    };
                },

                GroupTitle(gid, pid, data) =>
                {
                    if opts.topic_lock
                    {
                        drop(m.group_set_title(gid, &opts.topic_to_lock));
                    }
                }

                ev =>
                {
                    //println!("[DEBUG] {:?}", ev);
                },
            }
        }
        m.wait();
    }
}

fn init_tox() -> Tox
{
    // load Tox Profile
    let mut xp: Vec<u8> = Vec::new();

    let mut tox = match Tox::new(ToxOptions::new(), match File::open(Path::new(HFB_PROFILE))
    {
        Ok(mut data) =>
        {
            data.read_to_end(&mut xp).ok().expect("[ERROR] Impossible de lire le profil.");
            Some(&xp)
        },
        Err(_) => None
    })
    {
        Ok(t) => t,
        Err(err) => panic!("[ERROR] Tox InitError: {:?}", err)
    };

    let bootstrap_key = match BOOTSTRAP_KEY.parse()
    {
        Ok(b) => b,
        Err(_) => panic!("[ERROR] Erreur lors de la creation de la clef de bootstrap"),
    };

    // config + bootstrap
    tox.set_name("Hack-Free Bot").unwrap();
    tox.set_status_message("Développé par Skyzohkey et Ogromny").unwrap();
    tox.bootstrap(BOOTSTRAP_IP, BOOTSTRAP_PORT, bootstrap_key).unwrap();

    tox
}

fn save_point_tox(tox: &Tox)
{
    let path = Path::new(HFB_PROFILE);

    let mut file = match File::create(&path)
    {
        Err(e) => panic!("[ERROR] Impossible de créer le fichier \"{}\": {}", HFB_PROFILE, e),
        Ok(file) => file,
    };

    match file.write_all(&tox.save())
    {
        Ok(_) => println!("[DEBUG] Le fichier \"{}\" a été sauvegardé avec succés.", HFB_PROFILE),
        Err(e) => panic!("[ERROR] Impossible de sauvegarder le fichier \"{}\": {}", HFB_PROFILE, e),
    };
}

fn make_group_tox(tox: &mut Tox) -> i32
{
    let nbr = tox.add_groupchat().unwrap();
    drop(tox.group_set_title(nbr, "Hack-Free - Groupe officiel"));

    nbr
}

fn is_su(public_key: String) -> bool
{
    SUPER_USERS.contains(&&*public_key)
}
